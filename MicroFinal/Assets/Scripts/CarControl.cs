﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports; // Biblioteca para ler comunicação serial com Arduino

public class CarControl : MonoBehaviour {
	public float max_torque=250; // maior torque (evitar catar pneu e arrancada fraca)
	public float max_steer=30; // angulo máximo que a roda gira
	public Transform MeshRodaE, MeshRodaD;
	SerialPort serial = new SerialPort("COM5",9600); //define a porta

	public Light farolE, farolD;

	public Transform cam;	
	public float tiltAngle=180.0f;

	//wheel colliders
	public WheelCollider frontal_e;
	public WheelCollider frontal_d;
	public WheelCollider tras_e;
	public WheelCollider tras_d;
	// Use this for initialization
	void Start () {
		serial.Open ();
		serial.ReadTimeout = 1;
	}
	

	void FixedUpdate(){
		
		Vector3 pos;
		Quaternion rotate;
		float steer = Input.GetAxis ("Horizontal") * max_steer;//funciona tanto para w,a,s,d quanto setas direcionais e joysticks
		float torque= Input.GetAxis ("Vertical") * max_torque;

		Debug.Log ("Em cima" + serial.ReadLine ());
		string[] valores = serial.ReadLine ().Split ('#');
		steer= max_steer * ((float.Parse (valores [0]) - 505.0f) / 505.0f);
		torque = -1 * max_torque * ((float.Parse (valores [1]) - 483.0f) / 483.0f);

		float tiltAroundY =180.0f+ ((float.Parse(valores[3]) - 344.0f ) / 344.0f) * tiltAngle;
		float tiltAroundX = ((float.Parse(valores[4]) - 320.0f ) / 320.0f) * tiltAngle;
		Quaternion target = Quaternion.Euler(tiltAroundX, tiltAroundY, 0);
		cam.transform.localRotation = Quaternion.Slerp(transform.rotation, target,  3.0f);

		frontal_e.motorTorque = torque;
		frontal_d.motorTorque = torque;
		frontal_d.steerAngle = steer;
		frontal_e.steerAngle = steer;
		frontal_d.GetWorldPose (out pos, out rotate);
		MeshRodaE.rotation = rotate;
		MeshRodaD.rotation = rotate;

		if (int.Parse (valores [2]) == 0) {
			farolE.enabled = ! farolE.enabled;
			farolD.enabled = ! farolD.enabled;
		}

		serial.BaseStream.Flush ();

	}
}
